= Docker : Cheat Sheet

.Create a Container
[source, sh]
----
docker run myImage \
--detach \
-it \
--privileged \
--publish 8080:80 \
--name TotoContainer \
--restart always \
--volume /mnt/myVol/toto:/toto \
/bin/ls \
----

NOTE: Don't use `Ctrl + C` when attached to the main (lone) process of a
container or it'll kill the container (the container won't ave anymore running
process, it has no reason to stay up). If you want to quit a bash without
killing the container, use `Ctrl + P + Q` and you'll be detached from it.
Use `$ docker attach containerX` to re-attach. Or use
`$ docker exec containerX /bin/sh` to connect to a new shell that can be killed
without problem by a SIGINT.

.Other commands
[source, sh]
----
docker start stoppedContainer
docker kill startedContainer

# attach to the running process of a started container
docker attach startedContainer

docker exec -ti startedContainer /bin/bash

# list all containers
docker ps -a

docker rm stoppedContainer

docker commit Container newImage

docker images -a

docker volumes ls

# delete unused volumes
docker volumes prune

docker cp myContainer:/etc/myconfig C:/.../copiedConfig

docker stats runningContainer

docker inspect container
----